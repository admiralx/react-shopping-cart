import {useState} from 'react';
import {useQuery} from 'react-query';

//Mui
import {Badge, Drawer, Grid, LinearProgress} from '@material-ui/core';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';

//Styles
import {StyledButton, Wrapper} from './App.styles';

//Components
import Item from './Item/Item';
import Cart from './Cart/Cart';

//types
export type CartItemType = {
    id: number;
    category: string;
    description: string;
    image: string;
    price: number;
    title: string;
    amount: number;
}

const getProducts = async (): Promise<CartItemType[]> =>
    await (await fetch('https://fakestoreapi.com/products')).json();

const App = () => {

    const [cartOpen, setCartOpen] = useState(false);
    const [cartItems, setCartItems] = useState([] as CartItemType[]);

    const {data, isLoading, error} = useQuery<CartItemType[]>(
        'products',
        getProducts
    );

    const getTotalItems = (cartItems: CartItemType[]) =>
        cartItems.reduce((ack: number, item) => ack + item.amount, 0);

    const handleAddToCart = (clickedItem: CartItemType) => {
        setCartItems(prev => {
            // Is the item already added to the cart?
            const isItemAdded = prev.find(item => item.id === clickedItem.id);

            if (isItemAdded) {
                return prev.map(item =>
                    item.id === clickedItem.id
                        ? {...item, amount: item.amount + 1}
                        : item
                );
            }

            // First time the item is added
            return [...prev, {...clickedItem, amount: 1}];
        });
    };

    const handleRemoveFromCart = (id: number) => {
        setCartItems(prev => {
            const itemTobeRemoved = prev.find(item => item.id === id);

            if (!itemTobeRemoved) {
                alert("Item not found");
                return prev;
            }

            if (itemTobeRemoved.amount > 1) {
                return prev.map(item =>
                    item.id === id
                        ? {...item, amount: item.amount - 1}
                        : item
                );
            }

            return prev.filter(item => item.id !== id);
        })
    };

    if (isLoading) return <LinearProgress/>;
    if (error) return <div>Something Wen Wrong...</div>;

    return (
        <Wrapper>
            <Drawer anchor="right" open={ cartOpen } onClose={ () => setCartOpen(false) }>
                <Cart
                    addToCart={ handleAddToCart }
                    cartItems={ cartItems }
                    removeFromCart={ handleRemoveFromCart }
                />
            </Drawer>
            <StyledButton onClick={ () => setCartOpen(true) }>
                <Badge badgeContent={ getTotalItems(cartItems) } color='error'>
                    <AddShoppingCartIcon/>
                </Badge>
            </StyledButton>
            <Grid container spacing={ 3 }>
                { data?.map(item => (
                    <Grid item key={item.id} xs={12} sm={4}>
                        <Item item={item} handleAddToCart={handleAddToCart} />
                    </Grid>
                ))}
            </Grid>
        </Wrapper>
    );
}

export default App;
