import React from "react";

import {Button} from '@material-ui/core';

import {CartItemType} from "../App";
import {Wrapper} from "./CartItem.styles";

type Props = {
    count?: number;
    item: CartItemType;
    addToCart: (item: CartItemType) => void;
    removeCartITem: (id: number) => void;
}

const CartItem: React.FC<Props> = ({item, addToCart, removeCartITem}) =>
    <Wrapper>
        <div>
            <h3>{ item.title }</h3>
            <div className="information">
                <p>Price: ${ item.price }</p>
                <p>Total: ${ (item.price * item.amount).toFixed(2) }</p>
            </div>
            <div className="buttons">
                <Button
                    size="small"
                    disableElevation
                    variant="contained"
                    onClick={() => removeCartITem(item.id)}
                >
                    -
                </Button>
                <p>{item.amount}</p>
                <Button
                    size="small"
                    disableElevation
                    variant="contained"
                    onClick ={() => addToCart(item)}
                >
                    +
                </Button>
            </div>
        </div>
        <img src={item.image} alt={item.title}/>
    </Wrapper>
;

export default CartItem;